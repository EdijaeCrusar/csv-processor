package com.edijae.processor

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.edijae.processor.adapters.CSVAdapter
import com.edijae.processor.databinding.ActivityMainBinding
import com.edijae.processor.databinding.DialogBinding
import com.edijae.processor.utils.AppExecutors
import com.edijae.processor.utils.DialogUtils
import com.edijae.processor.utils.FileUtils
import com.edijae.processor.utils.PermissionUtils
import com.evrencoskun.tableview.pagination.Pagination
import com.opencsv.CSVReader
import com.opencsv.CSVWriter
import java.io.File
import java.io.FileReader
import java.io.FileWriter


class MainActivity : AppCompatActivity(), Pagination.OnTableViewPageTurnedListener {

    private lateinit var uri: Uri
    private var myEntries: MutableList<Array<String>>? = null
    private var path: String? = null
    private lateinit var progressDialog: ProgressDialog

    lateinit var binding: ActivityMainBinding
    lateinit var dialogBinding: DialogBinding
    private val READ_REQUEST_CODE = 42
    private val WRITE_STORAGE_REQUEST_CODE = 45

    private lateinit var adapter: CSVAdapter
    private lateinit var pagination: Pagination<Any>
    private var separator = ','
    private lateinit var separatorDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(binding.toolbar)
        adapter = CSVAdapter(this)
        binding.main = this
        binding.contentTbv.adapter = adapter
        binding.contentTbv.isShowVerticalSeparators = true
        binding.contentTbv.isShowHorizontalSeparators = true
        separatorDialog = Dialog(this)
        dialogBinding =
            DataBindingUtil.inflate<DialogBinding>(layoutInflater, R.layout.dialog, null, false)
        dialogBinding.main = this
        separatorDialog.setContentView(dialogBinding.root)
        separatorDialog.window?.attributes?.width = WindowManager.LayoutParams.MATCH_PARENT
        //pagination = Pagination(binding.contentTbv,15,this)

    }

    override fun onPageTurned(numItems: Int, itemsStart: Int, itemsEnd: Int) {

    }

    fun next(view: View) {
        pagination.nextPage()
    }

    fun back(view: View) {
        pagination.previousPage()
    }

    fun selectCSV(view: View) {
        if (PermissionUtils.isPermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            openCSV()
        } else {
            PermissionUtils.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                WRITE_STORAGE_REQUEST_CODE)
        }

    }

    /*
    * We use an intent to get the csv file from the device
    * */
    fun openCSV(){
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE).type = "text/comma-separated-values"

        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(
                Intent.createChooser(intent, "Select CSV file"), READ_REQUEST_CODE
            )
        } else {
            DialogUtils.showErrorDialog(
                getString(R.string.error_dialog_title), getString(R.string.no_app), this
            )
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.data?.let {
                separatorDialog.show()
                uri = it
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == WRITE_STORAGE_REQUEST_CODE &&
            PermissionUtils.isPermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            openCSV()
        }
    }
    fun validateSeparator(view: View) {
        if (dialogBinding.etSeparator.text.isNullOrEmpty()) {
            dialogBinding.etSeparator.error = "Please provide a column separator for the CSV file"
            return
        }
        separator = dialogBinding.etSeparator.text.toString().get(0)
        separatorDialog.dismiss()
        loadContent()
    }

    private fun showDialog(message: String) {
        progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.setMessage(message)
        progressDialog.show()
    }

    private fun dismissDialog() {
        progressDialog.dismiss()
    }


    /**
     * This method loads the csv file content to a list. We can't use java beans because we do not
     * have any prior knowledge of the scv columns which can be mapped into fields. Thus we store
     * column headers and all values as string objects
     * */
    private fun loadContent() {
        showDialog("Loading CSV data")
        AppExecutors.getInstance().diskIO().execute {
            path = FileUtils.getPath(this, uri)
            val csvFile = File(path)
            val reader = CSVReader(FileReader(csvFile)/*,separator, '\''*/)
            myEntries = reader.readAll()
            val headers = myEntries!![0]
            val rowHeaders = ArrayList<Int>()
            var pos = 1
            val content = myEntries?.subList(1, myEntries!!.size)!!.map {
                rowHeaders.add(pos)
                pos++
                it.toMutableList()
            }

            runOnUiThread {
                adapter.setAllItems(headers.toMutableList(), rowHeaders, content)
                //binding.llControls.visibility = View.VISIBLE
                binding.contentTbv.post {
                    binding.btnExport.visibility = View.VISIBLE
                    dismissDialog()
                }
            }

        }
    }

    /**
     * We save the content shown on the table back to the csv file
     * */
    fun export(view: View) {
        showDialog("Saving CSV data to ${path}")
        AppExecutors.getInstance().diskIO().execute {
            val writer = CSVWriter(FileWriter(path), separator)
            // feed in your array (or convert your data to an array)
            myEntries?.let {
                for (array in it) {
                    writer.writeNext(array)
                }
            }
            writer.close()
            runOnUiThread({
                dismissDialog()
                Toast.makeText(this, "CSV data successfully saved!", Toast.LENGTH_LONG).show()
            })
        }

    }

}
