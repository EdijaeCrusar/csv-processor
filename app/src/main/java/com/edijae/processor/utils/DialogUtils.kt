package com.edijae.processor.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.edijae.processor.R


object DialogUtils {

  fun showErrorDialog(title: String, message: String, context: Context) {
    val alert = AlertDialog.Builder(context)
      .setTitle(title)
      .setMessage(message)
      .setNeutralButton(context.getString(R.string.ok), null)
      .create()
    alert.show()
    val btn = alert.getButton(AlertDialog.BUTTON_NEUTRAL)
    val params = btn.layoutParams
    params.width = ViewGroup.LayoutParams.MATCH_PARENT
    btn.layoutParams = params
    btn.setTextColor(context.resources.getColor(R.color.black))
    hideProgressDialog()
  }

  fun showPermissionDialog(
    title: Int,
    message: Int,
    activity: Activity,
    permission: String,
    requestCode: Int,
    finish: Boolean
  ) {
    val alert = AlertDialog.Builder(activity)
      .setTitle(activity.getString(title))
      .setMessage(message)
      .setPositiveButton(R.string.grant
      ) { dialog, which ->
          dialog?.dismiss()
          PermissionUtils.requestPermission(activity, permission, requestCode)
      }
        .setNeutralButton(R.string.deny, object : DialogInterface.OnClickListener {
        override fun onClick(dialog: DialogInterface?, which: Int) {
          if (finish) {
            activity.finish()
          } else {
            dialog?.dismiss()
          }
        }

      })
      .create()
    alert.show()
    val pBtn = alert.getButton(AlertDialog.BUTTON_POSITIVE)
    pBtn.setTextColor(activity.resources.getColor(R.color.black))
    val nBtn = alert.getButton(AlertDialog.BUTTON_NEUTRAL)
    nBtn.setTextColor(activity.resources.getColor(R.color.black))
  }

  private var progressDialog: Dialog? = null


    fun hideProgressDialog() {
    progressDialog?.let {
      if(progressDialog!!.isShowing){
        progressDialog!!.dismiss()
      }
    }
  }
}