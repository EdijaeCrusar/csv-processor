package com.edijae.processor.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


/**
 * Created by crusar on 5/11/18.
 */
object PermissionUtils {

  fun isPermissionGranted(context: Context, permission: String): Boolean {
    val status = ContextCompat.checkSelfPermission(context, permission)

    if (status != PackageManager.PERMISSION_GRANTED) {
      return false
    }
    return true
  }

  fun grantedPermission(activity: Activity, permission: String, requestCode: Int): Boolean {
    val status = ContextCompat.checkSelfPermission(activity, permission)

    if (status != PackageManager.PERMISSION_GRANTED) {
      requestPermission(activity, permission, requestCode)
      return false
    }
    return true
  }

  fun requestPermission(activity: Activity, permission: String, requestCode: Int) {
    ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
  }
}