package com.edijae.processor.viewholders

import android.view.View
import android.widget.TextView
import com.edijae.processor.R
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

class CellViewHolder (val view: View) : AbstractViewHolder(view) {

    val cell_textview: TextView

    init {
        cell_textview = view.findViewById(R.id.cell_data)
    }
}