package com.edijae.processor.viewholders

import android.view.View
import android.widget.TextView
import com.edijae.processor.R
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

class ColumnHeaderViewHolder(var root: View) : AbstractViewHolder(root) {

    val header_textview: TextView

    init {
        header_textview = root.findViewById(R.id.cell_data)
    }
}