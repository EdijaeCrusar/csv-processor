package com.edijae.processor.viewholders

import android.view.View
import android.widget.TextView
import com.edijae.processor.R
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

class RowHeaderViewHolder (var root: View) : AbstractViewHolder(root) {

    val cell_textview: TextView

    init {
        cell_textview = root.findViewById(R.id.cell_data)
    }
}